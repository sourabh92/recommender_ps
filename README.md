# RecoEnginePS

 A one page applictaion that takes in a user id and shows a summary of similar users on click.
 Steps to get this running 

	1.Clone the source code from git clone https://sourabh92@bitbucket.org/sourabh92/recommender_ps.git
	2.Create a virtual environment in the cloned folder , activate it and install dependencies using requirements.txt [pip install requirements.txt]
	3.Set up a schema named recosys in a local MySQL server
	4.In the activated environment navigate to flaskapp folder and type flask push_to_db, this will create the tables in the data base, execute similarity operation and load similarities into database
	5.After database initialization navigate to flaskapp folder and type flask run
	6.Note that database initilaization take ~4 minutes as all the similarites are calculated and pushed  at once to make application flow faster
	
 Any configuration changes can be done in the props.py file 

